<html>
    <head>
        <title>Verdes Mares - <?php echo $__env->yieldContent('title'); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        
        <!-- Scripts -->
        <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
        
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <!-- Styles -->
        <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/intera-icons.css')); ?>" rel="stylesheet">
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('image/favicon.ico')); ?>">
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel sticky-top">
                <div class="container">
                    <a style="color: #12AADA; font-weight: bold;font-size: 30px; font-family:Comfortaa" class="navbar-brand" href="<?php echo e(url('/home')); ?>"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="'Toggle navigation'">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href=""> Tudo Sobre </a>
                            </li>
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   Matheus  <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">      
                                    <a class="dropdown-item" href="<?php echo e(url('/admin')); ?>">
                                        Painel Administrativo
                                    </a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#altera-senha-modal">
                                       Alterar Senha
                                    </a>
                                    <a class="dropdown-item" href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> sair</a>
                                    <form id="logout-form" action="" method="POST" style="display: none;">
                                      
                                    </form>
                                </div>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </nav>
            <main class="py-4">
                <?php echo $__env->yieldContent('content'); ?>
            </main>
            <div class="modal fade" data-url="" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="altera-senha-modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="" method="post">
                            <?php echo csrf_field(); ?>
                            <div class="modal-header bg-primary text-white">
                                <h4 class="modal-title" id="myModalLabel">Alterar Senha</h4>
                            </div>
                            <div class="modal-header">
                                <div class="container">
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="senha-atual">Senha Atual</label>
                                                <input required class="form-control" type="password" name="senhaAtual" id="senha-atual">
                                            </div>
                                            <div class="form-group">
                                                <label for="senha-nova">Senha Nova</label>
                                                <input required class="form-control" type="password" name="senhaNova" id="senha-nova">
                                            </div>
                                            <div class="form-group">
                                                <label for="confirmar-senha">Repetir Nova Senha</label>
                                                <input required class="form-control" type="password" name="confirmarSenha" id="confirmar-senha">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Alterar</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</html><?php /**PATH C:\develop\tudosobre\resources\views/layouts/app.blade.php ENDPATH**/ ?>