<div class="card p-4 shadow-sm">
    <h1 class="card-title mb-4">SEO</h1>
    <div class="card-body p-0">
        <input type="hidden" value="25" name="content_id">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="file-drop-area">

                        <div class="delete-button delete-button-title" id="delete-button-img-title">
                            <a><i class="icon-close"></i></a>
                        </div>

                        <div class="conteudoimg">
                            <img class="img_fake rounded float-left w-100" id="img_fake" src="<?php echo e(!empty($subject->thumbnail) ? $subject->thumbnail : old('thumbnail')); ?>">
                        </div>

                        <label class="fake-input" for="file-input"></label>

                        <span class="fake-btn"><i class="icon-plus"></i>Imagem</span>
                        <input class="file-input" type="file" id="file-input" onchange="readURL(this)";>
                    </div>
                </div>
            </div>


            <div class="col-sm-9">
                <div class="form-group">
                     <label class="label__bold">Titulo</label>
                     <input class="form-control <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" value="<?php echo e(!empty($subject->title) ? $subject->title : old('title')); ?>" name="title">
                     <div class="invalid-feedback"> <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?> <?php echo e($message); ?><?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?> </div>
                </div>
                <div class="form-group">
                    <label class="label__bold">Descrição</label>
                    <textarea  class="form-control <?php if ($errors->has('description')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('description'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="description"><?php echo e(!empty($subject->description) ? $subject->description : old('description')); ?></textarea>
                    <div class="invalid-feedback"> <?php if ($errors->has('description')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('description'); ?> <?php echo e($message); ?><?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?> </div>
                </div>
            </div>


            <input type="hidden"   id="image_content_base64"  value="<?php echo e(!empty($subject->thumbnail) ? $subject->thumbnail : old('thumbnail')); ?>" name="thumbnail">

            </div>
        </div>
</div>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_fake').attr('src', e.target.result);
                $('#image_content_base64').val(e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".icon-close").on('click', function () {
        $('#img_fake').attr('src',"");
        $('#image_content_base64').val("");
    });
</script><?php /**PATH C:\develop\tudosobre\resources\views/seo.blade.php ENDPATH**/ ?>