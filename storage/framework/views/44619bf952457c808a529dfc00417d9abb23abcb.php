<?php $__env->startSection('title', 'Tudo Sobre'); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <?php if(session('status')): ?>
       <?php $__env->startComponent('components.alert', ['type' => session('type')]); ?>
          <?php echo e(session('status')); ?>

       <?php echo $__env->renderComponent(); ?>
    <?php endif; ?>

    <div class="row justify-content-md-center criacaodeconteudo">
        <div class="col-sm-12 col-md-12">
            <div class="content"> 
                <form id="form-poll" method="post" action="<?php echo e($action); ?>">
                    <?php echo csrf_field(); ?>
                    <?php if(isset($_method)): ?><input type="hidden" name="_method" value="<?php echo e($_method); ?>"><?php endif; ?>
                    <div class="card bg-white p-4 shadow-sm">
                        <h1 class="card-title mb-4 p-0">Assunto</h1>

                        <div class="card-body p-0">
                            <input type="hidden" value="25" name="content_id">

                            <div class="form-group">
                                <label class="label__bold">Nome</label>
                                <input class="form-control <?php if ($errors->has('subject')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('subject'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" value="<?php echo e(!empty($subject->subject) ? $subject->subject : old('subject')); ?>" name="subject">
                                <div class="invalid-feedback"> <?php if ($errors->has('subject')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('subject'); ?> <?php echo e($message); ?><?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?> </div>
                            </div>

                            <div class="form-group">
                                <label class="label__bold">Slug</label>
                                <input class="form-control <?php if ($errors->has('slug')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('slug'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" value="<?php echo e(!empty($subject->slug) ? $subject->slug : old('slug')); ?>" name="slug">
                                <div class="invalid-feedback"> <?php if ($errors->has('slug')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('slug'); ?> <?php echo e($message); ?><?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?> </div>
                            </div>

                            <div class="form-group">
                                <label class="label__bold">Categoria</label>
                                <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="category">
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4"></div>

                    <?php echo $__env->make('seo', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary btn-large mr-2">Finalizar edição</button>
                        <a class="btn btn-outline-primary" href="/assunto">Cancelar</a>
                    </div>

                </form>

                <div class="mt-4"></div>

                <?php echo $__env->make('sites-table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\develop\tudosobre\resources\views/subject.blade.php ENDPATH**/ ?>