
<?php if(session('status_site')): ?>
   <?php $__env->startComponent('components.alert', ['type' => session('type_site')]); ?>
      <?php echo e(session('status_site')); ?>

   <?php echo $__env->renderComponent(); ?>
<?php endif; ?>

<div class="card p-4 shadow-sm">
    <h1 class="card-title mb-4">
        Sites 
        <a href="#" class="btn btn-large btn-primary btn-edit"><i class="icon-plus"></i>  </a>
    </h1>
    <div class="card-body p-0">
        <input type="hidden" value="25" name="content_id">
        <div class="row">
            <div class="table-responsive">
                <table id="table-sites" class="table table-hover mt-4 usertable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Endereço</th>
                            <th>Assuntos</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $subject->sites; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $site): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($site->name); ?></td>
                            <td><?php echo e($site->url); ?></td>
                            <td><?php echo e($site->countSubjects()); ?></td>
                            <td class="pull-right">
                                <button class="btn btn-primary mr-1 btn-edit pull-right" data-id="1">Editar</button>
                                <button class="btn btn-outline-primary mr-1 btn-confirm " data-url="https://intera.verdesmares.com.br/resetPassword/1">visuliazar</button>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

 <?php echo $__env->make('modal-site', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php /**PATH C:\develop\tudosobre\resources\views/sites-table.blade.php ENDPATH**/ ?>