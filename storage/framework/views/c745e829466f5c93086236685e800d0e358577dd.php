<?php $__env->startSection('title', 'Tudo Sobre'); ?>

<?php $__env->startSection('content'); ?>
<div class="container">

    <?php if(session('status')): ?>
       <?php $__env->startComponent('components.alert', ['type' => session('type')]); ?>
          <?php echo e(session('status')); ?>

       <?php echo $__env->renderComponent(); ?>
    <?php endif; ?>

	<div class="row btnsconteudo">
        <div class="col-sm-12">
            <a href="/assunto/create"class="btn btn-large btn-primary">
                <i class="icon-plus"></i> Novo Assunto
            </a>
             <a href="/categoria/create"class="btn btn-large btn-primary">
                <i class="icon-plus"></i> Nova Categoria
            </a>
        </div>
    </div>
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="mainconteudo" id="results-wrapper">
				<?php $__currentLoopData = $subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="col-md-12 conteudosingle">
					<div class="row">
						<div class="col-md-9 col-lg-10" data-toggle="modal" data-target="#modalVisualizar" onclick="" >
							<img class="conteudoimgthumb" src="<?php echo e($subject->thumbnail); ?>">
							<div class="infoconteudo" style="">
								<label class="label__uppercase">
									<?php $__currentLoopData = $subject->categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php echo e($category->name); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</label>
								<h3 class="titleconteudo__bold" style="font-family: 'Nunito', sans-serif;">
									<?php echo e($subject->subject); ?>

								</h3>
								<p class="paragraphconteudo">
									<?php echo e($subject->description); ?>

								</p>
								<div class="detailsconteudo">
									<label>
										Matheus Oliveira
									</label>
									<label>
										Widgets: 3
									</label>
									<label>
										Data de cadastro:
									</label>
								</div>
							</div>
						</div>
						<?php $__env->startComponent('components.action-buttons', ['object' => $subject]); ?><?php echo $__env->renderComponent(); ?>
					</div>
				</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				<div class="d-flex justify-content-center text-center" id="pagination-wrapper"></div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\develop\tudosobre\resources\views/index.blade.php ENDPATH**/ ?>