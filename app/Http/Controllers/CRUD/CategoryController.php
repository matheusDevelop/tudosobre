<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Subject;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category', ['action'=> '/categoria', 'subjects' => Subject::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'slug' => 'required|unique:categories',
        ]);

        if ($validator->fails()) {
            return redirect('categoria/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        return $this->_save($request, $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('category', ['action'=> '/categoria/'.$id, '_method'=> "PUT", 'category'=> Category::find($id), 'subject' => Subject::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'slug' => [
                'required',
                 Rule::unique('categories')->ignore($id),   
             ]
        ]);

        if ($validator->fails()) {
            return redirect("categoria/$id/edit")
                        ->withErrors($validator)
                        ->withInput();
        }

        return $this->_save($request, $category, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        $request->session()->flash('status', "Assunto $subject->subject removido com sucesso!");
        $request->session()->flash('type', 'success');

        return redirect('categoria');
    }

    public function _save($request, $category, $id = null)
    {
        $category->name = $request->name;
        $category->slug  = $request->slug;
        $category->save();

        $category->subjects()->sync($request->subject);
        $request->session()->flash('status',"Ação executada com sucesso!");
        $request->session()->flash('type', 'success');

        return redirect('categoria/' .$category->id. '/edit');
    }
}
