<?php

namespace App\Http\Controllers\CRUD;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Requests\SubjectRequest;
use Illuminate\Validation\Rule;
use App\Subject;
use App\Site;
use Illuminate\Support\Facades\Validator;

class SubjectController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index', ['subjects' => Subject::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subject', ['action'=> '/assunto', 'categories' => Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subject = new Subject();

        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'slug'  => 'required|unique:subjects',
            'title' => 'required',
            'description' => 'required',
            'thumbnail'
        ]);

        if ($validator->fails()) {
            return redirect('assunto/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        return $this->_save($request, $subject);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
        return view('subject', [
            'action' => '/assunto/'.$id, 
            '_method'=> "PUT", 
            'subject'=> Subject::find($id),
            'sites'  => Site::all(),  
            'categories' => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);

        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'slug' => [
                'required',
                 Rule::unique('subjects')->ignore($id),   
             ],
            'title' => 'required',
            'description' => 'required',
            'thumbnail'
        ]);

        if ($validator->fails()) {
            return redirect("assunto/$id/edit")
                        ->withErrors($validator)
                        ->withInput();
        }

        return $this->_save($request, $subject, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $subject = Subject::find($id);
        $subject->delete();

        $request->session()->flash('status', "Assunto $subject->subject removido com sucesso!");
        $request->session()->flash('type', 'success');

        return redirect('assunto');
    }

    public function _save($request, $subject, $id = null)
    {
        $subject->subject = $request->subject;
        $subject->slug  = $request->slug;
        $subject->title = $request->title;
        $subject->description = $request->description;
        $subject->thumbnail = $request->thumbnail;
        $subject->save();

        $subject->categories()->sync($request->category);
        $request->session()->flash('status',"Ação executada com sucesso!");
        $request->session()->flash('type', 'success');
        
       return redirect('assunto/' .$subject->id. '/edit');
    }
}
