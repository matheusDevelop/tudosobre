<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Requests\SiteRequest;
use App\Site;
use App\Subject;

class SiteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiteRequest $request)
    {
       
        return $this->_save($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteRequest $request, $id)
    {
         return $this->_save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $site = Site::find($id);
        $site->delete();

        return $this->sendResponse($site->toArray(), 'successfully.');
    }

    public function siteHasAssunto(Request $request){
        $subject = Subject::find($request->subject_id);
        $subject->sites()->sync($request->site_id);
    }

    public function _save($request, $id = null)
    {
        $validated = $request->validated();
        
        $site = ($id == null) ? new Site() : Site::find($id);

        $site->name = $request->name;
        $site->url  = $request->url;
        $site->save();

        $site->subjects()->sync($request->subject_id);

        $request->session()->flash('status_site',"Ação executada com sucesso!");
        $request->session()->flash('type_site', 'success');
        
        return $this->sendResponse($site->toArray(), 'successfully.');
    }
}
