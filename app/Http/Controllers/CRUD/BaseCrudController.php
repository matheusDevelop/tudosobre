<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\BaseController;


class BaseCrudController extends BaseController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request, $model)
    {
        $input = $request->all();

        $object = $model::create($input);

        return $model;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function _update($request, $model)
    {
        if(is_null($model)){
            return $this->sendError("Updated failed.");
        }

        $model->update($request->all());

        return $model;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function _destroy($model)
    {
        if(is_null($model)){
            return $this->sendError("Updated failed.");
        }
        $model->delete();

        return $this->sendResponse($model->toArray(), 'Deleted successfully');
    }
}
