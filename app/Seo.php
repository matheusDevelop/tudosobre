<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
     protected $fillable = [
       'slug', 'title', 'description','thumbnail'
    ];

    // Um seo pertence a um site
    public function site(){
        return $this->belongsTo('App\Site');
    }
}
