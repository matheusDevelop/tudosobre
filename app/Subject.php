<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'subject', 'slug', 'title', 'description','thumbnail'
    ];

    public function categories(){
		return $this->belongsToMany('App\Category');
	}

    public function sites(){
        return $this->belongsToMany('App\Site');
    }

    public function seo(){
        return $this->hasMany('App\Seo');
    }
}
