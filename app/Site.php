<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = ['name', 'url'];


    public function subjects(){
    	return $this->belongsToMany('App\Subject');
    }

    public function seo(){
        return $this->hasOne('App\Seo');
    }

    public function countSubjects(){
    	return $this->belongsToMany('App\Subject')->selectRaw('count(subjects.id) as aggregate')->groupBy('site_id')->count();
    }
}
