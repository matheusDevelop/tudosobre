@extends('layouts.app')

@section('title', 'Tudo Sobre')

@section('content')
<div class="container">

    @if(session('status'))
       @alert(['type' => session('type')])
          {{ session('status') }}
       @endalert
    @endif

	<div class="row btnsconteudo">
        <div class="col-sm-12">
            <a href="/assunto/create"class="btn btn-large btn-primary">
                <i class="icon-plus"></i> Novo Assunto
            </a>
             <a href="/categoria/create"class="btn btn-large btn-primary">
                <i class="icon-plus"></i> Nova Categoria
            </a>
        </div>
    </div>
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="mainconteudo" id="results-wrapper">
				@foreach ($subjects as $subject)
				<div class="col-md-12 conteudosingle">
					<div class="row">
						<div class="col-md-9 col-lg-10" data-toggle="modal" data-target="#modalVisualizar" onclick="" >
							<img class="conteudoimgthumb" src="{{$subject->thumbnail}}">
							<div class="infoconteudo" style="">
								<label class="label__uppercase">
									@foreach ($subject->categories as $category) {{$category->name}} @endforeach
								</label>
								<h3 class="titleconteudo__bold" style="font-family: 'Nunito', sans-serif;">
									{{$subject->subject}}
								</h3>
								<p class="paragraphconteudo">
									{{$subject->description}}
								</p>
								<div class="detailsconteudo">
									<label>
										Matheus Oliveira
									</label>
									<label>
										Widgets: 3
									</label>
									<label>
										Data de cadastro:
									</label>
								</div>
							</div>
						</div>
						@actionButtons(['object' => $subject])@endactionButtons
					</div>
				</div>
				@endforeach

				<div class="d-flex justify-content-center text-center" id="pagination-wrapper"></div>
			</div>
		</div>
	</div>
</div>
@endsection