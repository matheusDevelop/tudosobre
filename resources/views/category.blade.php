@extends('layouts.app')
@section('title', 'Tudo Sobre')
@section('content')
<div class="container">
    @if(session('status'))
       @alert(['type' => session('type')])
          {{ session('status') }}
       @endalert
    @endif
    <div class="row justify-content-md-center criacaodeconteudo">
        <div class="col-sm-12 col-md-8">
            <div class="content"> 
                <form id="form-poll" method="post" action="{{$action}}">
                    @csrf
                    @isset($_method)<input type="hidden" name="_method" value="{{$_method}}">@endisset
                    <div class="card bg-white p-4 shadow-sm">
                        <h1 class="card-title mb-4 p-0">Categoria</h1>

                        <div class="card-body p-0">
                            <input type="hidden" value="25" name="content_id">

                            <div class="form-group">
                                <label class="label__bold">Nome</label>
                                <input class="form-control @error('name') is-invalid @enderror" value="{{ !empty($category->name) ? $category->name : old('name') }}" name="name">
                                <div class="invalid-feedback"> @error('name') {{$message}}@enderror </div>
                            </div>

                            <div class="form-group">
                                <label class="label__bold">Slug</label>
                                <input class="form-control @error('slug') is-invalid @enderror" value="{{ !empty($category->slug) ? $category->slug : old('slug') }}" name="slug">
                                <div class="invalid-feedback"> @error('slug') {{$message}}@enderror </div>
                            </div>

                        </div>
                    </div>

                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary btn-large mr-2">Finalizar edição</button>
                        <a class="btn btn-outline-primary" href="/assunto">Cancelar</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection
