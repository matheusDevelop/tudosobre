
@if(session('status_site'))
   @alert(['type' => session('type_site')])
      {{ session('status_site') }}
   @endalert
@endif

<div class="card p-4 shadow-sm">
    <h1 class="card-title mb-4">
        Sites 
        <a href="#" class="btn btn-large btn-primary btn-edit"><i class="icon-plus"></i>  </a>
    </h1>
    <div class="card-body p-0">
        <input type="hidden" value="25" name="content_id">
        <div class="row">
            <div class="table-responsive">
                <table id="table-sites" class="table table-hover mt-4 usertable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Endereço</th>
                            <th>Assuntos</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subject->sites as $site)
                        <tr>
                            <td>{{$site->name}}</td>
                            <td>{{$site->url}}</td>
                            <td>{{$site->countSubjects()}}</td>
                            <td class="pull-right">
                                <button class="btn btn-primary mr-1 btn-edit pull-right" data-id="1">Editar</button>
                                <button class="btn btn-outline-primary mr-1 btn-confirm " data-url="https://intera.verdesmares.com.br/resetPassword/1">visuliazar</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

 @include('modal-site')

