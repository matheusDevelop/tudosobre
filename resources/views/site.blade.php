@extends('layouts.app')
@section('title', 'Tudo Sobre')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="form-poll" method="post" action="/poll_edit">
                        <input type="hidden" name="_token" value="UedKgi0baE47Cgeimdl8aUiMiubsWH7q3tvy4nVh">
                        <div class="row criacaodeconteudo">
                            <div class="col-md-8">
                                <h1 class="title--smaller">Site</h1>
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" value="25" name="content_id">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <div class="file-drop-area">
                                                                <div class="delete-button delete-button-title" id="delete-button-img-title">
                                                                    <a>
                                                                        <i class="icon-close"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="conteudoimg">
                                                                    <img class="img_fake" id="img_fake" data-item="title">
                                                                </div>
                                                                <label class="fake-input" for="file-input"></label>
                                                                <span class="fake-btn">
                                                                    <i class="icon-plus"></i>Imagem
                                                                </span>
                                                                <input class="file-input" type="file" id="file-input" onchange="readURL(this, '#image_content_base64', '#img_fake');">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="form-group">
                                                            <label class="label__bold">Nome</label>
                                                            <input class="form-control" value="" name="title">
                                                            <label class="label__bold">Endereço</label>
                                                            <input class="form-control" name="url">
                                                            <label class="label__bold">Endereço da Página de Assunto</label>
                                                            <input class="form-control" name="url">
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" value="" id="image_content_base64" name="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header bg-primary text-white" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Assuntos
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body card-body-sidbar">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                                                                <option value="">Selecione</option>
                                                                <option value="1">Futebol</option>
                                                                <option value="2">Política</option>
                                                            </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         @include('seo')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection