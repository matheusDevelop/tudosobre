<div class="row justify-content-md-center">
    <div class="col-sm-12 col-md-8">
        <div class="alert alert-{{$type}}" role="alert">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		    {{$slot}} 
		</div>
    </div>
</div>