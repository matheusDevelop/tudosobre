<div class="modal fade" data-id="" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="form-modal">
    <div class="modal-dialog modal-lg" style="width: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Criar Site</h4>
                </div>
                <div class="modal-body">
                    <form action="/site" class="form-sites">
                        @csrf
                        <input type="hidden" name="subject_id" value="{{$subject->id}}" />
                        <div class="form-group row">
                            <label for="name" class="col-md-12 col-form-label">Nome</label>

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name">
                                <div class="invalid-feedback"> o campo Nome é obrigatório </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-form-label">Endereço</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="url">
                                <div class="invalid-feedback"> o campo url obrigatório </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <a href="#" class="btn btn-primary btn-block btn-form-sites">
                                    adicionar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"> ou usar um já cadastrado</h4>
                </div>
                <div class="modal-body">
                    <form action="/site/assunto" class="form-sites">
                        @csrf
                        <input type="hidden" name="subject_id" value="{{$subject->id}}" />
                        <div class="form-group row">
                            <div class="col-md-12">
                                <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="site_id">
                                    <option value="0">Selecione</option>
                                    @foreach ($sites as $site)
                                        <option value="{{$site->id}}">{{$site->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <a href="#" class="btn btn-primary btn-block btn-form-sites">
                                    Relacionar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
<script type="text/javascript">
$(".btn-edit").on("click", function(){
        //id = $(this).attr('data-id');
        //name = $(this).parent("td").parent("tr").find('td').html();
        //email = $(this).parent("td").parent("tr").find('td').eq(1).html();
        //type = $(this).parent("td").parent("tr").find('td').eq(2).html();

        //$("#form-modal input[name='id']").val(id);
        //$("#form-modal input[name='name']").val(name);
        //$("#form-modal input[name='email']").val(email);
        //$("#form-modal select").val(type);
        $("#form-modal").modal('show');
    });

$('.btn-form-sites').click(function(){
    
    action = $(this).closest('.form-sites').attr('action');

    console.log(action)

    name = $("#form-modal input[name='name']").val();
    url  = $("#form-modal input[name='url']").val();
    
    subject_id = $("#form-modal input[name='subject_id']").val();
    site_id    = $("#form-modal select[name='site_id']").val();

    $.ajax({  
        method: "POST", 
        url: action, 
        data: {name: name, url: url, subject_id: subject_id, site_id: site_id },
        statusCode: {
        422: function() {
            if(name == "")
                $("#form-modal input[name='name']").addClass('is-invalid');
            
            if(url == "")
                $("#form-modal input[name='url']").addClass('is-invalid');
            
        },
        200: function() {
           window.location.href = "/assunto/{{$subject->id}}/edit#table-sites";
           location.reload();
        }
    }
    }).done(function( msg ) {});
});

    
</script>