<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('site', 'CRUD\SiteController');
Route::post('site/assunto', 'CRUD\SiteController@siteHasAssunto');

Route::resource('categoria', 'CRUD\CategoryController');
Route::resource('assunto', 'CRUD\SubjectController');


